package ejercicio3;

public class ejercicio3 {

	public static void main(String[] args) {
		/*
		3 - Escribe un programa Java que realice lo siguiente: declarar dos variables X e Y de tipo int,
		dos variables N y M de tipo double y asigna a cada una un valor. A continuación muestra por
		pantalla:
			• El valor de cada variable.
			• La suma X + Y
			• La diferencia X – Y
			• El producto X * Y
			• El cociente X / Y
			• El resto X % Y
			• La suma N + M
			• La diferencia N – M
			• El producto N * M
			• El cociente N / M
			• El resto N % M
			• La suma X + N
			• El cociente Y / M
			• El resto Y % M
			• El doble de cada variable
			• La suma de todas las variables
			• El producto de todas las variables  
		*/

		// Declaración de las variables
		
			int X = 14;
			
			int Y = 6;
			
			double N = 2.67;
			
			double M = 7.89;
		
		// Mostrar por pantalla
			
			System.out.println("Valor de X: " + X);
			
			System.out.println("Valor de Y: " + Y);
			
			System.out.println("Valor de N: " + N);
			
			System.out.println("Valor de M: " + M);
		
			
			System.out.println("Suma de X e Y: " + (X + Y));
			
			System.out.println("Diferencia de X e Y: " + (X - Y));
			
			System.out.println("Producto de X e Y: " + (X * Y));
			
			System.out.println("Cociente de X e Y: " + (X / Y));
			
			System.out.println("Resto de X e Y: " + (X % Y));
			
			System.out.println("Suma de N y M: " + (N + M));
			
			System.out.println("Diferencia de N y M: " + (N - M));
			
			System.out.println("Producto de N y M: " + (N * M));
			
			System.out.println("Cociente de N y M: " + (N / M));
			
			System.out.println("Resto de N y M: " + (N % M));
			
			System.out.println("Suma de X y N: " + (X + N));
			
			System.out.println("Cociente de Y y M: " + (Y / M));
			
			System.out.println("Resto de Y y M: " + (Y % M));
			
			System.out.println("Doble de X: " + (2 * X));
			
			System.out.println("Doble de Y: " + (2 * Y));
			
			System.out.println("Doble de N: " + (2 * N));
			
			System.out.println("Doble de M: " + (2 * M));
			
			System.out.println("Suma de todas las variables: " + (X + Y + N + M));
			
			System.out.println("Producto de todas las variables: " + (X * Y * N * M));
			
	}

}
