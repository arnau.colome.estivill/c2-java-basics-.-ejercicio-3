# C2 Java Basics . Ejercicio 3

3 - Escribe un programa Java que realice lo siguiente: declarar dos variables X e Y de tipo int,
dos variables N y M de tipo double y asigna a cada una un valor. A continuación muestra por
pantalla:
    • El valor de cada variable.
    • La suma X + Y
    • La diferencia X – Y
    • El producto X * Y
    • El cociente X / Y
    • El resto X % Y
    • La suma N + M
    • La diferencia N – M
    • El producto N * M
    • El cociente N / M
    • El resto N % M
    • La suma X + N
    • El cociente Y / M
    • El resto Y % M
    • El doble de cada variable
    • La suma de todas las variables
    • El producto de todas las variables